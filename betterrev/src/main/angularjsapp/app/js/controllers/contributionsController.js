betterrevApp.controller('ContributionsController', function ($scope, contributionsService) {

    betterrevApp.config(['$resourceProvider', function ($resourceProvider) {
            // Don't strip trailing slashes from calculated URLs
            $resourceProvider.defaults.stripTrailingSlashes = false;
        }]);

    $scope.contributions = contributionsService.getAll();
    
    // TODO pass in the actual id
    $scope.contribution = contributionsService.get(1);
});
