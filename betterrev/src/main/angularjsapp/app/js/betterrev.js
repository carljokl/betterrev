'use strict';

// Create an app called betterrev and pull in ngRouter and translate modules
var betterrevApp = angular.module('betterrev', [ 'ngRoute', 'pascalprecht.translate', 'ui.bootstrap', 'ngResource']);

(function() {

    // Configure routing for the main app
    betterrevApp
        .config([
                '$routeProvider',
                function($routeProvider) {

                    $routeProvider.when('/index', {
                        templateUrl : 'partials/home.html'
                    }).when('/contributions', {
                        templateUrl : 'partials/contributions.html'
                    }).when('/contributions/:contributionId', {
                        templateUrl : 'partials/contribution.html'
                    }).when('/help', {
                        templateUrl : 'partials/help.html'
                    }).when('/contactus', {
                        templateUrl : 'partials/contactus.html'
                    }).otherwise({
                        redirectTo : '/partials/home.html'
                    });
                } ]);

    /* TODO shift to its own file */
    /*
     * var betterrevAdminApp = angular.module('betterrevAdmin', ['ngRoute']);
     * 
     * betterrevAdminApp.config([ '$routeProvider', function($routeProvider) {
     * $routeProvider.when('tags', { templateUrl : '/tags.html' }).
     * }).otherwise({ redirectTo : '/admin.html' });
     * 
     * 
     * }]);
     */

})();
