package org.adoptopenjdk.betterrev.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Set;

import javax.persistence.*;

/**
 * Entity that represents a concept of Interest within Betterrev.
 */
@Entity
@Table(name="Interest")
@NamedQueries({
        @NamedQuery(name = "Interest.findAll", query = "SELECT interest FROM Interest interest")
})
public class Interest {

    // TODO get by ID the Java EE Way
    // public static Finder<Long, Interest> find = new Finder<>(Long.class, Interest.class);

    @GeneratedValue(strategy = IDENTITY)
    @Id
    private Long id;

    private String path;

    private String project;

    public Interest() {}
    
    public Interest(String path, String project) {
        this.path = path;
        this.project = project;
    }

    @Override
    public String toString() {
        return "Interest [id=" + id + ", path=" + path + ", project=" + project + "]";
    }

    public boolean caresAbout(String repository, Set<String> filePaths) {
        for (String filePath : filePaths) {
            if (repository.equals(project) && filePath.matches(this.path)) {
                return true;
            }
        }
        return false;
    }

    public String getPath() {
        return path;
    }

    public String getProject() {
        return project;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}